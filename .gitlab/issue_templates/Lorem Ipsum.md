# Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dictum orci lectus, suscipit malesuada quam mollis ut. Quisque tempor nec elit sed faucibus. Aliquam ligula orci, accumsan ut lorem dictum, tempus dapibus ipsum. Vestibulum cursus enim vel odio fringilla bibendum. Nunc egestas nec velit vitae scelerisque. Pellentesque molestie congue nibh quis interdum. Nullam at fermentum nisl, id semper sapien. Aliquam erat volutpat. Sed a odio eu est pellentesque vulputate ut quis leo. Nulla quis dapibus quam.

1. Chocolate
  - dark
  - milk

Etiam laoreet sit amet dolor at venenatis. Phasellus id blandit risus. Nulla et turpis lectus. Vivamus eleifend rutrum pretium. Sed porta felis non massa cursus blandit. Pellentesque malesuada vestibulum vehicula. Vivamus lobortis in elit eget hendrerit.

Sometimes you want to :monkey: around a bit and add some :star2: to your :speech_balloon:. Well we have a gift for you:

:zap: You can use emoji anywhere GFM is supported. :v:

You can use it to point out a :bug: or warn about :speak_no_evil: patches. And if someone improves your really :snail: code, send them some :birthday:. People will :heart: you for that.

If you're new to this, don't be :fearful:. You can join the emoji :family:. All you need to do is to look up one of the supported codes.

Consult the [Emoji Cheat Sheet](https://www.emojicopy.com) for a list of all supported emoji codes. :thumbsup:

Proin tristique leo dolor, id sagittis mauris tincidunt vel. Curabitur ac posuere sem, at rutrum urna. Sed pretium nulla sit amet felis accumsan, quis rhoncus lacus interdum. Integer aliquam laoreet venenatis. Suspendisse tincidunt fermentum feugiat. Duis eget metus justo. Morbi orci dolor, accumsan facilisis rhoncus a, finibus lobortis felis. Quisque tincidunt pellentesque lobortis. Duis euismod nibh et purus tristique lacinia. Duis vel posuere felis. Donec id convallis enim, a consectetur diam. Integer in quam pharetra, finibus augue quis, auctor lectus. Integer odio tellus, aliquam sit amet elementum at, faucibus sed enim.

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "SubGraph 1 Flow"
  SubGraph1Flow(SubNode 1)
  SubGraph1Flow -- Choice1 --> DoChoice1
  SubGraph1Flow -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[Node 1] --> Node2[Node 2]
  Node2 --> SubGraph1[Jump to SubGraph1]
  SubGraph1 --> FinalThing[Final Thing]
end
```

Quisque ultricies nisl pellentesque cursus scelerisque. Sed odio sapien, mattis et porttitor vitae, rhoncus ut orci. Ut in purus vitae velit sollicitudin porttitor elementum vel sem. Cras eget ante eget urna rutrum convallis eget eget est. Mauris sit amet condimentum est. Donec id ligula sed turpis sollicitudin auctor quis mollis arcu. Etiam sed semper velit. Duis iaculis eros non libero posuere, at auctor quam malesuada. Quisque sed libero fermentum, iaculis velit id, blandit ex. Donec lobortis porttitor eros, quis tempus ante malesuada vel. Nullam non sagittis justo.

- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3

1. [x] Completed task
1. [ ] Incomplete task
   1. [ ] Sub-task 1
   1. [x] Sub-task 2

In nec tellus volutpat, dapibus elit quis, rhoncus elit. Proin sed urna imperdiet erat tempor volutpat. Vestibulum quis est ante. Morbi fringilla consectetur gravida. Suspendisse tincidunt urna id felis mattis egestas. Nulla leo tortor, viverra id nibh blandit, volutpat semper nunc. Mauris eget euismod massa. Integer ut auctor felis. Morbi metus massa, gravida sit amet dignissim et, bibendum id tellus. Pellentesque sed elit leo. Suspendisse maximus libero ut ultrices ultricies. Vestibulum at accumsan nisi.

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

```ruby
require 'redcarpet'
markdown = Redcarpet.new("Hello World!")
puts markdown.to_html
```

```
No language indicated, so no syntax highlighting.
s = "There is no highlighting for this."
But let's throw in a <b>tag</b>.
```


| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |